# blogs

This repository contains two blog series - one on Ansible and one on UrbanCode Deploy (UCD). The Ansible blog is 9 part and the UCD blog is 11 parts. There is also a directory containing a presentation from ZTech Connect 2019 with embedded videos demonstrating the UCD tool. Lastly, there is a directory containing a devops training course I wrote for a large health care client.
