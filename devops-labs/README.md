# Devops Training With Labs

This repository contains a 25 part training course written for a large healthcare client. The content is docker driven and consists of lab exercises stored as PDf documents.
While the lab focusses on UCD, it does cover creating devops pipeline using Bitbucket, Maven, Artifactory, and Jenkins. Care should be used when cutting and pasting from PDFs into UCD. THe figures for
the laps are also stored seperately in a subfolder for reuse.